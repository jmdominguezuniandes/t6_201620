�Qu� pasa si llega nueva informaci�n para una llave que ya existe en la tabla de hash?
R:Si llega informaci�n para una llave que ya existe en la tabla Hash, se busca la 
posici�n en la cu�l se encuentre dicha llave y se reemplaza el valor que la que estaba
previamente almacenado con la llave.

�Cu�l es el criterio de crecimiento de la tabla?
R: El m�todo resize se llama desde el m�todo put si el factor de carga actual supera
el factor de carga m�xima. Cada vez que esto ocurra se realizan los cambios respectivos
para que la capacidad de la tabla sea el doble. Es decir que el crecimiento de la tabla se da
en un factor de 2, cada vez que se supera el factor de carga m�ximo.

�Cu�l funci�n de hashing escogi� y por qu�?
R: La funci�n de hashing escogida fue:
int indice = Integer.valueOf(llave.hashCode()) % capacidad;
		if(indice < 0){
			indice = indice * -1;
		}
		return indice;
Est� funci�n se escogi� puesto que el m�todo hashCode retorna un entero, facilitando
la conversi�n de la llave a un n�mero. Adicionalmente se realiza el m�dulo de la capacidad
para que el valor obtenido se encuentre entre los indices de la tabla. Finalmente se realiza
la verificaci�n de que el valor sea mayor que uno, puesto que el m�todo hashCode puede retornar
valor negativos, en ese caso es necesario multiplicarlo por -1, para que el valor se encuentre 
entre los �ndices de la tabla.

8. Mida el tiempo de ejecuci�n de los requerimientos R1, R2 para tablas de hash con 500000, 
1000000, 1500000 y 2000000 de datos. Haga una tabla de comparaci�n de tiempos de R1 y R2 para cada 
tabla de hash definida �Qu� opina de estos resultados? �Si agrega m�s datos los tiempos de ejecuci�n cambian significativamente? 
Responda y justifique estas preguntas en el archivo README.txt
R: Al realizar la comprobaci�n de los requerimientos R1 y R2, los resultados de los tiempos de ejecuci�n
fueron muy r�pidos, por su parte, al aumentar el n�mero de datos los tiempos de ejecuci�n no
cambiaron significativamente. Esto se debe a que la tabla realiza accesos directamente con numeros
enteros al arreglo y a que el factor de carga no permite que las lista de la tabla se hagan muy largas. Adicionalmente
esto demuestra que la funci�n de hash fu� escogida adecuadamente.

�Tiene sentido hacer una tabla de listas?
R: Si tiene sentido hacer una tabla de listas, puesto que es necesario
retornar todas la personas con determinado nombre o apellido(La llave no es �nica)
por lo tanto se asegura que todas las personas con el mismo nombre o apellido
van a guardarse en la misma lista. Sin embargo, es necesario tener en cuenta que 
estad�sticamente existe la probabilidad de que en la lista tambi�n queden personas
con otros apellido, por lo cual antes de dar la respuesta es necesario validar cuales personas
tienen el apellido que lleg� por par�metro.

�Cuando las tablas de Hash no son una buena opci�n?
R: Las tablas de hash no son una buena opci�n cuando los datos de entrada no
permiten que las duplas llave, valor se distribuyan uniformemente a lo largo de la
tabla puesto que las listas(En separate chaining), o el n�mero de objetos seguidos(En linear probing)
puede volverse muy grande, aumentando el orden de b�squeda y de inserci�n de los
objetos.

Nota: La estructura LinkedList utilizada, fue tomada del paquete de estructuras
env�ado por el profesor Mario Linares V�squez a trav�s de SicuaPlus, esta estructura
es OpenSource y tiene licencia MIT. La estructura fue modificada para que en lugar
de soportar Nodos normales, soporte NodosHash, adicionalmente se le a�adi� el m�todo
get.