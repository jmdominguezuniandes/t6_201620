package test;

import taller.Mundo.Ciudadano;
import taller.Mundo.DirectorioEmergencias;
import junit.framework.TestCase;

public class DirectorioTest extends TestCase {

	private DirectorioEmergencias directorio;
	
	public void setupEscenario1(){
		directorio = new DirectorioEmergencias();
	}
	
	public void testAnadirYBuscar(){
		
		setupEscenario1();
		
		directorio.anadirCiudadano("1", "Juan", "Rodríguez", "2 Grados Norte 30 Grados Oriente");
		directorio.anadirCiudadano("2", "Jorge","Mendez", "5 grados norte, 3 grados occidente");
		directorio.anadirCiudadano("3", "Miguel", "Perez","20 Grados sur 5 Grados Oriente");
		directorio.anadirCiudadano("4", "Ignacio", "Arizmendi", "1 Grado Norte 5 Grados occidente");
		directorio.anadirCiudadano("5", "José", "Marulanda","90 Grados norte 5 Gradps oriente");
		
		Ciudadano ciudadano1 = directorio.buscarCiudadano("1");
		Ciudadano ciudadano2 = directorio.buscarCiudadano("2");
		Ciudadano ciudadano3 = directorio.buscarCiudadano("3");
		Ciudadano ciudadano4 = directorio.buscarCiudadano("4");
		Ciudadano ciudadano5 = directorio.buscarCiudadano("5");
		
		assertEquals("El teléfono 1 corresponde a Juan", "Juan", ciudadano1.darNombre());
		assertEquals("El teléfono 2 corresponde a Jorge", "Jorge", ciudadano2.darNombre());
		assertEquals("El teléfono 3 corresponde a Miguel", "Miguel", ciudadano3.darNombre());
		assertEquals("El teléfono 4 corresponde a Ignacio", "Ignacio", ciudadano4.darNombre());
		assertEquals("El teléfono 5 corresponde a José", "José", ciudadano5.darNombre());
		
	}
	
	public void testBuscarPorApellido(){
		setupEscenario1();

		directorio.anadirCiudadano("1", "Juan", "Perez", "2 Grados Norte 30 Grados Oriente");
		directorio.anadirCiudadano("2", "Jorge","Mendez", "5 grados norte, 3 grados occidente");
		directorio.anadirCiudadano("3", "Miguel", "Perez","20 Grados sur 5 Grados Oriente");
		directorio.anadirCiudadano("4", "Ignacio", "Arizmendi", "1 Grado Norte 5 Grados occidente");
		directorio.anadirCiudadano("5", "José", "Perez","90 Grados norte 5 Gradps oriente");
		directorio.anadirCiudadano("6", "Javier", "Perez", "1 Grado sur 6 Grados occidente");
		
		Object[] lista = directorio.darListaCiudadanosPorApellido("Perez");
		for(int i = 0; i < lista.length; i++){
			Ciudadano actual = (Ciudadano) lista[i];
			assertEquals("En la lista únicamente deberían haber ciudadanos de apellido Perez", "Perez", actual.darApellido());
		}
		
		assertEquals("En la lista deberían haber 4 ciudadanos", 4, lista.length);
	}
	
	public void testBuscarPorLocalizacion(){
		setupEscenario1();

		directorio.anadirCiudadano("1", "Juan", "Perez", "2 Grados Norte 30 Grados Oriente");
		directorio.anadirCiudadano("2", "Jorge","Mendez", "5 grados norte, 3 grados occidente");
		directorio.anadirCiudadano("3", "Miguel", "Perez","20 Grados sur 5 Grados Oriente");
		directorio.anadirCiudadano("4", "Ignacio", "Arizmendi", "1 Grado Norte 5 Grados occidente");
		directorio.anadirCiudadano("5", "José", "Perez","90 Grados norte 5 Grados oriente");
		directorio.anadirCiudadano("6", "Javier", "Perez", "1 Grado sur 6 Grados occidente");
		
		Ciudadano ciudadano1 = (Ciudadano) directorio.BuscarPorLocalizacion("2 Grados Norte 30 Grados Oriente");
		Ciudadano ciudadano2 = (Ciudadano) directorio.BuscarPorLocalizacion("5 grados norte, 3 grados occidente");
		Ciudadano ciudadano3 = (Ciudadano) directorio.BuscarPorLocalizacion("20 Grados sur 5 Grados Oriente");
		Ciudadano ciudadano4 = (Ciudadano) directorio.BuscarPorLocalizacion("1 Grado Norte 5 Grados occidente");
		Ciudadano ciudadano5 = (Ciudadano) directorio.BuscarPorLocalizacion("90 Grados norte 5 Grados oriente");
		Ciudadano ciudadano6 = (Ciudadano) directorio.BuscarPorLocalizacion("1 Grado sur 6 Grados occidente");
		
		assertEquals("El ciudadano 1 debería ser juan", "Juan", ciudadano1.darNombre());
		assertEquals("El ciudadano 2 debería ser jorge", "Jorge", ciudadano2.darNombre());
		assertEquals("El ciudadano 3 debería ser miguel", "Miguel", ciudadano3.darNombre());
		assertEquals("El ciudadano 4 debería ser ignacio", "Ignacio", ciudadano4.darNombre());
		assertEquals("El ciudadano 5 debería ser josé", "José", ciudadano5.darNombre());
		assertEquals("El ciudadano 6 debería ser javier", "Javier", ciudadano6.darNombre());
	}
}
