 package test;

import taller.estructuras.TablaHash;
import junit.framework.TestCase;

public class HashTableTest extends TestCase {
	
	private TablaHash tabla;
	
	public void setupEscenario1(){
		tabla = new TablaHash();
	}
	
	public void testAnadirYBuscar(){
		
		setupEscenario1();
		
		tabla.put("Hola", "Primero");
		tabla.put("Amigo", "Segundo");
		tabla.put("Chepe", "Tercero");
		tabla.put("Hola", "Cuarto");
		
		assertEquals("MensajeError", "Segundo",  tabla.get("Amigo"));
		assertEquals("MensajeError", "Tercero", tabla.get("Chepe"));
		assertEquals("MensajeError", "Cuarto", tabla.get("Hola"));
		
	}
	
	public void testEliminar(){
		setupEscenario1();
		tabla.put("Hola", "Primero");
		tabla.put("Amigo", "Segundo");
		tabla.put("Chepe", "Tercero");
		tabla.put("Hola", "Cuarto");
		tabla.delete("Amigo");
		
		assertEquals("MensajeError", null,  tabla.get("Amigo"));
		assertEquals("MensajeError", "Tercero", tabla.get("Chepe"));
		assertEquals("MensajeError", "Cuarto", tabla.get("Hola"));
	}

}
