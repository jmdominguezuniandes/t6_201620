package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import taller.Mundo.Ciudadano;
import taller.Mundo.DirectorioEmergencias;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";
	private static DirectorioEmergencias directorio;

	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio t
			directorio = new DirectorioEmergencias();
			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				//TODO: Agrege los datos al directorio de emergencias
				String localizacion = "";
				String nombre = datos[0];
				String apellido = datos[1];
				String telefono = datos[2];
				if(datos.length > 3){
					localizacion = datos[3];
				}
				
				directorio.anadirCiudadano(telefono, nombre, apellido, localizacion);
				//Recuerde revisar en el enunciado la estructura de la información
				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");
				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia
					System.out.println("Ingrese el nombre del ciudadano");
					String nombre = br.readLine();
					System.out.println("Ingrese el apellido del ciudadano");
					String apellido = br.readLine();
					System.out.println("Ingrese el telefono del ciudadano");
					String telefono = br.readLine();
					System.out.println("Ingrese la localizacion del telefono");
					String localizacion = br.readLine();
					directorio.anadirCiudadano(telefono, nombre, apellido,localizacion);
					break;
				case "2":
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					System.out.println("Ingrese el teléfono del ciudadano a buscar");
					String telefono1 = br.readLine();
					Ciudadano buscado = directorio.buscarCiudadano(telefono1);
					if(buscado == null){
						System.out.println("No existe un ciudadano con el telefono dado");
					}
					else{
						String nombreI = buscado.darNombre();
						String apellidoI = buscado.darApellido();
						String telefonoI = buscado.darTelefono();
						String localizacionI = buscado.darLocalizacion();
						System.out.println("Los datos del ciudadano buscado son:");
						System.out.println();
						System.out.println("Nombre: " + nombreI);
						System.out.println();
						System.out.println("Apellido: " + apellidoI);
						System.out.println();
						System.out.println("Telefono: " + telefonoI);
						System.out.println();
						System.out.println("Localización: " +  localizacionI);
					}
					break;
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					System.out.println("Ingrese el apellido del ciudadano");
					String apellidoABuscar = br.readLine();
					Ciudadano lista[] = directorio.darListaCiudadanosPorApellido(apellidoABuscar);
					if(lista.length == 0){
						System.out.println("No existen ciudadanos con ese apellido");
					}
					else{
						System.out.println("Los ciudadanos con el apellido " + apellidoABuscar + " son:" );
						System.out.println();
						for(int i = 0; i < lista.length; i++){
							Ciudadano temp = lista[i];
							System.out.println("Nombre: " + temp.darNombre() + " " + "Apellido: " + temp.darApellido() + " " + "Telefono: " + temp.darTelefono() + " " + temp.darLocalizacion());
						}
					}
					break;
				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					System.out.println("Ingrese la localización del ciudadano");
					String localizacionABuscar = br.readLine();
					Ciudadano buscado1 = directorio.BuscarPorLocalizacion(localizacionABuscar);
					
					if(buscado1 == null){
						System.out.println("No existe un ciudadano con la localizacion");
					}
					else{
						String nombreB = buscado1.darNombre();
						String apellidoB = buscado1.darApellido();
						String telefonoB = buscado1.darTelefono();
						System.out.println("Los datos del ciudadano buscado son:");
						System.out.println();
						System.out.println("Nombre: " + nombreB);
						System.out.println();
						System.out.println("Apellido: " + apellidoB);
						System.out.println();
						System.out.println("Telefono: " + telefonoB);
					}
					
					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

}
