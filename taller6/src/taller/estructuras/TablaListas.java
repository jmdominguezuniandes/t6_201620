package taller.estructuras;

public class TablaListas <K extends Comparable<K> ,V>{

	
	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private LinkedList[] arregloDeBusqueda;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	
	public TablaListas(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		capacidad = 500000;
		count = 0;
		arregloDeBusqueda = new LinkedList[500000];
		factorCarga = 0;
		factorCargaMax = 4;
		for(int i = 0; i < arregloDeBusqueda.length; i++){
			arregloDeBusqueda[i] = new LinkedList();
		}
		
	}

	@SuppressWarnings("unchecked")
	public TablaListas(int pCapacidad, float pFactorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		capacidad = pCapacidad;
		count = 0;
		arregloDeBusqueda = new LinkedList[capacidad];
		factorCarga = 0;
		factorCargaMax = pFactorCargaMax;
		for(int i = 0; i < arregloDeBusqueda.length; i++){
			arregloDeBusqueda[i] = new LinkedList();
		}

	}
	
	public void put(K llave, V valor){
		int indice = hash(llave);
		LinkedList lista = arregloDeBusqueda[indice];
		lista.add(llave, valor);
		count ++;
		recalcularFactorCarga();
		
			
		if(factorCarga > 4){
			reSize(capacidad * 2);
		}
	}
	
	private void recalcularFactorCarga(){
		factorCarga = count/capacidad;
	}
	
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		int indice = Integer.valueOf(llave.hashCode()) % capacidad;
		if(indice < 0){
			indice = indice * -1;
		}
		return indice;
	}
	
	private void reSize(int nuevaCapacidad){
		TablaListas<K ,V> t;
		t = new TablaListas<K, V>(nuevaCapacidad, factorCargaMax);
		for(int i = 0; i < arregloDeBusqueda.length; i++){
			LinkedList actual = arregloDeBusqueda[i];
			for(int j = 0; j < actual.getSize(); j++){
				K llave = (K) actual.getLlave(j);
				V valor = (V) actual.getValor(j);
				t.put(llave, valor);
			}
		}
		factorCarga = t.factorCarga;
		factorCargaMax = t.factorCargaMax;
		arregloDeBusqueda = t.arregloDeBusqueda;
		count = t.count;
		capacidad = t.capacidad;
		
	}
	
	public Object[] buscar(K llave){
		Object[] buscado = null;
		
		int indice = hash(llave);
		LinkedList temp = arregloDeBusqueda[indice];
		buscado = new Object[temp.getSize()];
		int j = 0;
		for(int i = 0; i < temp.getSize(); i++){
			if(temp.getLlave(i).equals(llave)){
				buscado[j] = temp.getValor(i);
				j++;
			}
		}
		
		return buscado;
	}
	
	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones

		V buscado = null;
		
		int indice = hash(llave);
		LinkedList actual = arregloDeBusqueda[indice];
		for(int i = 0; i < actual.getSize(); i++){
			if(llave.equals(actual.getLlave(i))){
				buscado = (V) actual.getValor(i);
				actual.deleteAtK(i);
			}
		}
		
		if(buscado != null){
			recalcularFactorCarga();
		}
		
		return buscado;
	}


}
