package taller.estructuras;


public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private LinkedList arregloDeBusqueda[];

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;
	

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		capacidad = 500000;
		count = 0;
		arregloDeBusqueda = new LinkedList[500000];
		factorCarga = 0;
		factorCargaMax = 4;
		for(int i = 0; i < arregloDeBusqueda.length; i++){
			arregloDeBusqueda[i] = new LinkedList();
		}
		
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int pCapacidad, float pFactorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		capacidad = pCapacidad;
		count = 0;
		arregloDeBusqueda = new LinkedList[pCapacidad];
		factorCarga = 0;
		factorCargaMax = pFactorCargaMax;
		for(int i = 0; i < arregloDeBusqueda.length; i++){
			arregloDeBusqueda[i] = new LinkedList();
		}

	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		NodoHash aAgregar = new NodoHash(llave, valor);
		boolean llaveExistente = false;
		int indice = hash(llave);
		LinkedList lista = arregloDeBusqueda[indice];
		for(int i = 0; i < lista.getSize(); i++){
			if(lista.getLlave(i).equals(llave)){
				lista.deleteAtK(i);
				lista.add(llave, valor);
				llaveExistente = true;
				break;
			}
		}
		
		if(llaveExistente == false){
			lista.add(llave, valor);
			count ++;
			recalcularFactorCarga();
			
			if(factorCarga > 4){
				reSize(capacidad * 2);
			}
		}
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V buscado = null;

		int indice = hash(llave);
		LinkedList actual = arregloDeBusqueda[indice];
		for(int i = 0; i < actual.getSize(); i++){
			if(actual.getLlave(i).equals(llave)){
				buscado = (V) actual.getValor(i);
				break;
			}
		}
		
		return buscado;

	}

	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones


		V buscado = null;
		
		int indice = hash(llave);
		LinkedList actual = arregloDeBusqueda[indice];
		for(int i = 0; i < actual.getSize(); i++){
			if(llave.equals(actual.getLlave(i))){
				buscado = (V) actual.getValor(i);
				actual.deleteAtK(i);
			}
		}
		
		if(buscado != null){
			count --;
			recalcularFactorCarga();
		}
		
		return buscado;
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		int indice = Integer.valueOf(llave.hashCode()) % capacidad;
		if(indice < 0){
			indice = indice * -1;
		}
		return indice;
	}
	
	private void recalcularFactorCarga(){
		factorCarga = count/capacidad;
	}
	
		//TODO: Permita que la tabla sea dinamica
	private void reSize(int nuevaCapacidad){
		TablaHash<K ,V> t;
		t = new TablaHash<K, V>(nuevaCapacidad, factorCargaMax);
		for(int i = 0; i < arregloDeBusqueda.length; i++){
			LinkedList actual = arregloDeBusqueda[i];
			for(int j = 0; j < actual.getSize(); j++){
				K llave = (K) actual.getLlave(j);
				V valor = (V) actual.getValor(j);
				t.put(llave, valor);
			}
		}
		factorCarga = t.factorCarga;
		factorCargaMax = t.factorCargaMax;
		arregloDeBusqueda = t.arregloDeBusqueda;
		count = t.count;
		capacidad = t.capacidad;
		
	}

}