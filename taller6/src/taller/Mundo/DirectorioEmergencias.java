package taller.Mundo;

import taller.estructuras.TablaHash;
import taller.estructuras.TablaListas;

public class DirectorioEmergencias {

	private TablaHash tablaEmergencias;
	private TablaListas tablaListas;
	private TablaHash tablaLocalizacion;
	
	public DirectorioEmergencias(){
		tablaEmergencias = new TablaHash();
		tablaListas = new TablaListas();
		tablaLocalizacion = new TablaHash(100000, 10);
	}
	
	public void anadirCiudadano(String pTelefono, String pNombre, String pApellido, String pLocalizacion){
		Ciudadano aAgregar = new Ciudadano(pNombre, pApellido, pTelefono,pLocalizacion);
		Ciudadano aAgregar2 = new Ciudadano(pNombre, pApellido, pTelefono, pLocalizacion);
		tablaEmergencias.put(pTelefono, aAgregar);
		tablaListas.put(pApellido, aAgregar);
		tablaLocalizacion.put(pLocalizacion, aAgregar2);
	}
	
	public Ciudadano buscarCiudadano(String pTelefono){
		Ciudadano respuesta = (Ciudadano) tablaEmergencias.get(pTelefono);
		return respuesta;
	}
	
	public Ciudadano[] darListaCiudadanosPorApellido(String pApellido){
		Object lista[] = tablaListas.buscar(pApellido);
		int contador = 0;
		for(int i = 0; i<lista.length; i++){
			if(lista[i] != null){
				contador ++;
			}
		}
		int j = 0;
		Ciudadano respuesta[] = new Ciudadano[contador];
		for(int i = 0; i< lista.length; i++){
			Ciudadano ciudadano = (Ciudadano) lista[i];
			if(ciudadano != null){
				respuesta[j] = ciudadano;
				j++;
			}
		}
		
		return respuesta;
	}
	
	public Ciudadano BuscarPorLocalizacion(String pLocalizacion){
		Ciudadano respuesta = (Ciudadano) tablaLocalizacion.get(pLocalizacion);
		return respuesta;
	}
	
	
	
}
