package taller.Mundo;

public class Ciudadano {

	
	private String primerNombre;
	private String apellido;
	private String numeroTelefono;
	private String localizacion;
	
	public Ciudadano(String pNombre, String pApellido, String pTelefono){
		primerNombre = pNombre;
		apellido = pApellido;
		numeroTelefono = pTelefono;
		localizacion = "";
	}
	
	public Ciudadano(String pNombre, String pApellido, String pTelefono, String pLocalizacion){
		primerNombre = pNombre;
		apellido = pApellido;
		numeroTelefono = pTelefono;
		localizacion = pLocalizacion;
	}
	
	
	public String darNombre(){
		return primerNombre;
	}
	
	public String darApellido(){
		return apellido;
	}
	
	public String darTelefono(){
		return numeroTelefono;
	}
	
	public String darLocalizacion(){
		return localizacion;
	}
}
